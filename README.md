# Getting Started

## Clone & install project dependencies
1. Clone the boilerplate `git@gitlab.com:mrkong.villanueva/cms-boilerplate.git`
2. Go to `platform` folder
3. Run `composer install ` to install dependencies

## Running Project
1. On Root Folder
3. Run `docker-compose up --build` to build and run the project

## Links 
- PHPMyAdmin http://localhost:8080
- Project Link http://localhost

## PHPMyAdmin
1. visit http://localhost:8080
2. login using `admin` as username and `Abc123` for password
3. Click login

## Connect Databse from Workbench
1. Open Workbench
2. input `127.0.0.1` as host
3. input `admin` as username
4. input `Abc123` as password


## AdminLTE 
- https://adminlte.io/themes/v3